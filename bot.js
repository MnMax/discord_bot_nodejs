const Discord = require('discord.js')
const request = require('request')
const bot = new Discord.Client()
const tokens = require('./tokens.json');


// Debugging the bot status.
bot.on('ready', function () {
  console.log("Je suis connecté !")
})

// Welcoming new members.
bot.on('guildMemberAdd', member => {
  member.createDM().then(channel => {
    return channel.send('Bienvenue sur le serveur ' + member.displayName)
  }).catch(console.error)
})

bot.on('message', message => {
  // Channel clearing function.
  if (message.content == "!clear") {
    if (!message.channel.permissionsFor(message.author).hasPermission("MANAGE_MESSAGES")) {
      message.channel.sendMessage("Désolé, vous n'avez pas la permission d'éxécuter la commande : \""+message.content+"\"");
      console.log("Désolé, vous n'avez pas la permission d'éxécuter la commande : \""+message.content+"\"");
      return;
    } else if (!message.channel.permissionsFor(bot.user).hasPermission("MANAGE_MESSAGES")) {
      message.channel.sendMessage("Désolé, vous n'avez pas la permission d'éxécuter la commande : \""+message.content+"\"");
      console.log("Désolé, vous n'avez pas la permission d'éxécuter la commande : \""+message.content+"\"");
      return;
    }
    if (message.channel.type == 'text') {
      message.channel.fetchMessages()
        .then(messages => {
          message.channel.bulkDelete(messages);
          messagesDeleted = messages.array().length;
          message.channel.send("Suppression terminée, nombre de messages supprimés : "+messagesDeleted);
        })
        .catch(err => {
          console.log('Erreur lors de la suppression');
          console.log(err);
        });
    }
  }

  // Latency test with a ping/pong.
  if (message.content == '!ping') {
    message.channel.send('pong!')
  }

  // Weather.
  if (message.content.startsWith('!meteo')) {
    let apiKey = tokens["apiKey"];
    let args = message.content.split(' ')
    let city = args[1];
    let url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${apiKey}&units=metric`

    request(url, function (err, response,body){
      if(err){
        console.log("Error : ",err);
      }
      else{
        let weather = JSON.parse(body);
        try {
          let toSend = `Il fait ${weather.main.temp} degrés à ${weather.name}!`;
          //message.channel.send(toSend);
          message.channel.send({embed: {
              color: 3447003,
              title: `Voici la météo pour ${weather.name} :sun_with_face:`,
              fields: [{
                  name: `:thermometer: TEMPERATURE :thermometer:` ,
                  value: `**Actuel** : ${weather.main.temp} c° \n**Min** : ${weather.main.temp_min} c° \n**Max** : ${weather.main.temp_max} c°`
                },
                {
                  name: ":white_sun_rain_cloud: ETAT :white_sun_rain_cloud: ",
                  value: `**Global** : ${weather.weather[0].main} \n**Pression** : ${weather.main.pressure} hPa \n**Humidité** : ${weather.main.humidity} %`
                },
                {
                  name: ":wind_blowing_face: VENT :wind_blowing_face:",
                  value: `**vitesse** : ${weather.wind.speed} m/s \n**Orientation** : ${weather.wind.deg}° `
                },
                {
                  name: ":map: INFOS VILLE :map: ",
                  value: `**Nom** : ${weather.name} \n**Lon** : ${weather.coord.lon} \n**Lat** : ${weather.coord.lat}`
                },
                {
                  name: ":sunny: INFOS SOLEIL :sunny:",
                  value: `**Lever** : ${timeConverter(weather.sys.sunrise)} \n**Coucher** : ${timeConverter(weather.sys.sunset)}`
                }
              ],
              timestamp: new Date(),
            }
          });
        } catch (e) {
          message.channel.send("Désolé, la météo n'a pas pu être récupérée...");
        }
      }
    });
  }

  if(message.content.startsWith('!music')){
    let args = message.content.split(' ');
    if(args[1] == "play"){
      console.log("PLAY");
    }
    else if(args[1] == "stop"){
      console.log("STOP");
    }
  }
});




function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
  return time;
}




bot.login(tokens["discord"])
